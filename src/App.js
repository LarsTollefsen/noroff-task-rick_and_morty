import React from 'react';
import './App.css';
import CharacterList from './components/CharacterList/CharacterList';
import CharacterPage from './components/CharacterPage/CharacterPage';
import { BrowserRouter as Router, Switch, Route, useParams} from "react-router-dom"

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={CharacterList} />
          <Route path="/character/:id" component={CharacterPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
