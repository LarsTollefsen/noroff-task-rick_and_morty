import React from "react";
import styles from "./Character.module.css";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent'; 
import {BrowserRouter as Router, Link } from "react-router-dom"

function Character(passed) {
    return (
        <div className={styles.characterCardContainer}>
            <Card className={styles.characterCard}>
                <CardContent>
                    <CardHeader titleTypographyProps={{variant:'h6'}} title={passed.character.name} />
                    <CardMedia style={{paddingTop: '100%'}} image={passed.character.image} title={passed.character.name} />
                        <Link to={{ pathname: `/character/${passed.character.id}`, state: { character: passed } }} className={styles.characterCard}>
                            <div className={styles.cardButton}>
                                <Button variant="outlined" color="default">More information</Button>
                            </div>
                        </Link>
                </CardContent>
            </Card>
        </div>
    );
};

export default Character;