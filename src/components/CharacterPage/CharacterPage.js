import React from "react";
import styles from "./CharacterPage.module.css";

class CharacterPage extends React.Component {
    // Initialize the State in Class Component.
    constructor(props) {
        super(props)
        this.routeParam = props.match.params.id
        this.state = {
          character: []
        };
    }

    async componentDidMount() {
    try {
        const response = await fetch("https://rickandmortyapi.com/api/character/" + this.routeParam).then(resp => resp.json()).then(data => this.setState({character: data}))
    } catch (e) {
        console.error(e);
    }
  }
    render() {
        return (
            <div>
                <h1>Name: {this.state.character.name}</h1>
                <img src={this.state.character.image}></img>
                <h3>Status: {this.state.character.status}</h3>
                <h3>Species: {this.state.character.species}</h3>
                <h3>Gender: {this.state.character.gender}</h3>
                <h3>Created: {this.state.character.created}</h3>
            </div>
        );
    }
}  

export default CharacterPage;