import React from "react";
// Import the CSS file as a module.
import styles from "./CharacterList.module.css";
import Character from "../Character/Character"
import TextField from '@material-ui/core/TextField'; 

// Constant To store API url.
const API_URL = "https://rickandmortyapi.com/api/character/";


class CharacterList extends React.Component {
  // Initialize the State in Class Component.
  constructor(props) {
    super(props)
    this.state = {
      characters: [],
      filtered: []
    };  
    this.handleChange = this.handleChange.bind(this);
  }



  // Use ASYNC/AWAIT inside lifecycle method.
  async componentDidMount() {
    try {
      
      const response = await fetch(API_URL).then(resp => resp.json());
      // Add the current characters to the chars array.
      let chars = [...this.state.characters];
      // Add the results from the API response.
      chars.push(...response.results);

      // ALWAYS use this.setState() in a Class Method.
      this.setState({
        characters: chars,
        filtered: chars
      });
    } catch (e) {
      console.error(e);
    }
  }

  handleChange(e) {
    let currentList = [];
    let newList = [];

    if (e.target.value !== "") {
      currentList = this.state.characters;

      newList = currentList.filter(item => {
        const lc = item.name.toLowerCase();
        const filter = e.target.value.toLowerCase();

        return lc.includes(filter);
      });
    } else {
      newList = this.state.characters
    }
    this.setState({
      filtered: newList
    });
  };

  // Required render() method in Class Component.
  render() {
    // Create an array of JSX to render
    const characters = this.state.filtered.map((character, index) => {
      // This should render Character components. - Remember the key.
      return (
        <Character key={index} character={character} />
      );
    });

    // Render MUST return valid JSX.
    return (
      <div>
        <div className={styles.CharacterList}>
          <h1>Rick &amp; Morty</h1>
          <div className={styles.SearchBarContainer}>
            <TextField onChange={this.handleChange} variant="outlined" margin="dense" className={styles.SearchBar} label="Search for a character"></TextField>
          </div>
            <div className="Characters">{characters}</div>
        </div>
      </div>
    );
  }
}

export default CharacterList;
